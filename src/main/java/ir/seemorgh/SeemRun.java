package ir.seemorgh;


import ir.seemorgh.persistence.daoImpls.PersonDaoImpl;
import ir.seemorgh.persistence.maps.PersonEntity;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author rootnt
 */
public class SeemRun {
//    @Autowired
//    private static SessionFactory sf;
    public static void main(String[] args) {
        
        
//        System.out.println(sf.toString());
        ApplicationContext context = new ClassPathXmlApplicationContext("spring_context.xml");
        
        PersonDaoImpl a = (PersonDaoImpl) context.getBean(PersonDaoImpl.class);
//        PersonDaoImpl ej = new PersonDaoImpl();
        PersonEntity pers = new PersonEntity();
        pers.setAddr("gew vwer ve");
        pers.setId("fe", "joojoo", "0912121212");
        pers.setBrthDate("13920220");
        pers.setEmail("eege");
        pers.setNationalCode("1271111111");
        a.addObject(pers);
//        PersonEntity person = a.addObject(pers);
        System.out.println("done");
    }
}