package ir.seemorgh.configFiles;


/**
 *
 * @author rootnt
 */

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import com.mysql.jdbc.Driver;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import org.springframework.orm.hibernate3.AbstractSessionFactoryBean;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.persistence.EntityManagerFactory;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;

@Configuration
@ComponentScan("ir.seemorgh")
@EnableTransactionManagement
public class SpringJavaConfig {
    @Bean
    public DataSource dataSource() {
        
        SimpleDriverDataSource dataSource = null;
        try {
            dataSource = new SimpleDriverDataSource(
                new Driver(),
                "jdbc:mysql://localhost:3306/seemorghdb ",
                "seemorgh",
                "11"
            );
        } catch (SQLException e) {
            System.out.println("****Driver Problem****");
            System.out.println(e.getMessage());
//            System.exit(0);
        }
        return dataSource;
    }
    
//    private String [] packageToScan = {"ir.seemorgh.persistence.maps"};
    private Properties hibProperties () {
        Properties hibProp = new Properties();
        hibProp.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        hibProp.put("hibernate.show_sql", "true");
        hibProp.put("hibernate.current_session_context_class", "thread");
        return hibProp;
    }
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(dataSource());
//        em.setPackagesToScan(new String[] {"ir.seemorgh.persistence.maps"});
//
//        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        em.setJpaVendorAdapter(vendorAdapter);
//        em.setJpaProperties(hibProperties());
//
//        return em;
//    }
    
//    @Bean
//    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(emf);
//
//        return transactionManager;
//    }
//    
//    @Bean
//    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
//        return new PersistenceExceptionTranslationPostProcessor();
//    }
    
//    Properties additionalProperties() {
//      Properties properties = new Properties();
//      properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
//      properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
//      return properties;
//   }
//    @Bean
//    public SessionFactory sessionFactory() throws Exception {
//        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
//        sessionFactoryBean.setDataSource(dataSource());
//        sessionFactoryBean.setPackagesToScan(packageToScan);
//        sessionFactoryBean.setHibernateProperties(hibProperties());
//        sessionFactoryBean.afterPropertiesSet();
//        return sessionFactoryBean.getObject();
//}
    
    @Bean
    public LocalSessionFactoryBean sessionFactory() throws SQLException , IOException {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan(new String[] {"ir.seemorgh.persistence.maps"});
        sessionFactoryBean.setHibernateProperties(hibProperties());
        sessionFactoryBean.afterPropertiesSet();
        return sessionFactoryBean;
    }
    
//    @Bean
//    public SessionFactory sessionFactory() throws SQLException{
//        SessionFactory ret = sessionFactoryBean().getObject();
//        return ret;
//    }
}
