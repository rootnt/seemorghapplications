package ir.seemorgh.persistence.DAO;


/**
 *
 * @author rootnt
 */
public interface DataBaseDAO<T> {
    public void addObject(T obj);
    public void deleteObject(T obj);
    public void updateObject(T obj);
//    public Object getObject(Object... args);
}
