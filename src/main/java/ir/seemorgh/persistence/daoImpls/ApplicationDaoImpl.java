package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.ApplicationEntity;
import ir.seemorgh.persistence.maps.ApplicationPK;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class ApplicationDaoImpl implements DataBaseDAO<ApplicationEntity>{
    private SessionFactory sessionFactory;
    
    private Session currentSession(){
        return sessionFactory.getCurrentSession();
    }
        
    public ApplicationEntity getApplication(String name, String family, String tel, int appCode) {
        ApplicationPK applicationPK = new ApplicationPK(name, family, tel, appCode);
        return (ApplicationEntity) currentSession().get(ApplicationEntity.class, applicationPK);
    }
    
    @Override
    public void updateObject(ApplicationEntity application) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(application);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(ApplicationEntity application) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(application);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(ApplicationEntity application) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(application);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}
