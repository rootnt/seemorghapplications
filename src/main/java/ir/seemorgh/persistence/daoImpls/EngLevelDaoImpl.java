package ir.seemorgh.persistence.daoImpls;


import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.EngLevelEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class EngLevelDaoImpl implements DataBaseDAO<EngLevelEntity>{
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public EngLevelEntity getPerson(int appCode) {
        return (EngLevelEntity) currentSession().get(EngLevelEntity.class, appCode);
    }
    
    @Override
    public void updateObject(EngLevelEntity engLevel) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(engLevel);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(EngLevelEntity engLevel) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(engLevel);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(EngLevelEntity engLevel) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(engLevel);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}
