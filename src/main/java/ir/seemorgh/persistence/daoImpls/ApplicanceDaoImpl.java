package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.ApplicanceEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class ApplicanceDaoImpl implements DataBaseDAO<ApplicanceEntity>{
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public ApplicanceEntity getApplicance(int appCode) {
        return (ApplicanceEntity) currentSession().get(ApplicanceEntity.class, appCode);
    }
    
    @Override
    public void updateObject(ApplicanceEntity Applicance) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(Applicance);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(ApplicanceEntity Applicance) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(Applicance);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(ApplicanceEntity Applicance) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(Applicance);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}
