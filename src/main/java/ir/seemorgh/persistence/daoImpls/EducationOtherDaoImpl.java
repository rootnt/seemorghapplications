package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.EducationOtherEntity;
import ir.seemorgh.persistence.maps.EducationOtherPK;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class EducationOtherDaoImpl implements DataBaseDAO<EducationOtherEntity>{
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public EducationOtherEntity getPerson(int eduOrder, int appCode) {
//        PersonEntity person = new PersonEntity();
//        person.getId(name, family, tel)
        EducationOtherPK eduOthPK = new EducationOtherPK(eduOrder, appCode);
//        Transaction tx = currentSession().getTransaction();
        return (EducationOtherEntity) currentSession().get(EducationOtherEntity.class, eduOthPK);
    }
    
    @Override
    public void updateObject(EducationOtherEntity eduOth) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(eduOth);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(EducationOtherEntity eduOth) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(eduOth);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(EducationOtherEntity eduOth) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(eduOth);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}
