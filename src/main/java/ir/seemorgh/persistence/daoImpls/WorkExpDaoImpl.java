package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.WorkExperienceEntity;
import ir.seemorgh.persistence.maps.WorkExperiencePK;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class WorkExpDaoImpl implements DataBaseDAO<WorkExperienceEntity>{
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public WorkExperienceEntity getPerson(int jobOrder, int appCode) {
//        PersonEntity person = new PersonEntity();
//        person.getId(name, family, tel)
        WorkExperiencePK expPK = new WorkExperiencePK(jobOrder, appCode);
//        Transaction tx = currentSession().getTransaction();
        return (WorkExperienceEntity) currentSession().get(WorkExperienceEntity.class, expPK);
    }
    
    @Override
    public void updateObject(WorkExperienceEntity workExp) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(workExp);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(WorkExperienceEntity workExp) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(workExp);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(WorkExperienceEntity workExp) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(workExp);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}
