package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.EducationEntity;
import ir.seemorgh.persistence.maps.EducationPK;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class EducationDaoImpl implements DataBaseDAO<EducationEntity>{
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public EducationEntity getPerson(int appCode, int lvl) {
//        PersonEntity person = new PersonEntity();
//        person.getId(name, family, tel)
        EducationPK educationPK = new EducationPK(appCode, lvl);
//        Transaction tx = currentSession().getTransaction();
        return (EducationEntity) currentSession().get(EducationEntity.class, educationPK);
    }
    
    @Override
    public void updateObject(EducationEntity edu) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(edu);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(EducationEntity edu) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(edu);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(EducationEntity edu) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(edu);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}
