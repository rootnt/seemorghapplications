package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.PersonEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ir.seemorgh.persistence.maps.PersonPK;
import org.hibernate.Transaction;


/**
 *
 * @author rootnt
 */
@Repository
public class PersonDaoImpl implements DataBaseDAO<PersonEntity> {
    @Autowired
//    @Qualifier("springJavaConfig.sessionFactory")
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public PersonEntity getPerson(String name, String family, String tel) {
//        PersonEntity person = new PersonEntity();
//        person.getId(name, family, tel)
        PersonPK personPK = new PersonPK(name, family, tel);
//        Transaction tx = currentSession().getTransaction();
        return (PersonEntity) currentSession().get(PersonEntity.class, personPK);
    }
    
    @Override
    public void updateObject(PersonEntity person) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(person);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(PersonEntity person) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(person);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(PersonEntity person) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(person);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}