package ir.seemorgh.persistence.daoImpls;

import ir.seemorgh.persistence.DAO.DataBaseDAO;
import ir.seemorgh.persistence.maps.AvailableJobsEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rootnt
 */
@Repository
public class AvailableJobsDaoImpl implements DataBaseDAO<AvailableJobsEntity> {
    private SessionFactory sessionFactory;
    
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
        
    public AvailableJobsEntity getjob(int jobCode) {
        return (AvailableJobsEntity) currentSession().get(AvailableJobsEntity.class, jobCode);
    }
    
    @Override
    public void updateObject(AvailableJobsEntity job) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().saveOrUpdate(job);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void deleteObject(AvailableJobsEntity job) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().delete(job);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
    
    @Override
    public void addObject(AvailableJobsEntity job) {
        Transaction tx = null;
        try {
            tx = currentSession().beginTransaction();
            currentSession().save(job);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            sessionFactory.close();
        }
    }
}