package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author rootnt
 */
@Embeddable
public class EducationPK implements Serializable {
    @Column(name = "app_code", nullable = false)
    int appCode;
    
    @Column(name = "lvl", nullable = false)
    int lvl;

    public EducationPK() {
    }

    public EducationPK(int appCode, int lvl) {
        this.appCode = appCode;
        this.lvl = lvl;
    }
    
    
}
