package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author rootnt
 */
@Entity
@Table(name = "work_exp")
public class WorkExperienceEntity implements Serializable{
    @EmbeddedId
    WorkExperiencePK id;
    
    @Column(name = "inst_name", nullable = false)
    String instName;
    
    @Column(name = "addr")
    String addr;
    
    @Column(name = "from_date")
    String fromDate;
    
    @Column(name = "to_date")
    String toDate;

    @Column(name = "task")
    String task;
    
    @Column(name = "related", nullable = false)
    boolean related;
    
    @Column(name = "duration")
    int duration;

    
    
    public WorkExperiencePK getId() {
        return id;
    }

    public void setId(WorkExperiencePK id) {
        this.id = id;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public boolean isRelated() {
        return related;
    }

    public void setRelated(boolean related) {
        this.related = related;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

}
