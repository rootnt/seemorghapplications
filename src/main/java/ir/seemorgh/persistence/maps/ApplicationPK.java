package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author rootnt
 */
@Embeddable
public class ApplicationPK implements Serializable{
    @Column(name = "name", nullable = false)
    String name;
    
    @Column(name = "family", nullable = false)
    String family;
    
    @Column(name = "tel", nullable = false)
    String tel;
    
    @Column(name = "app_code", nullable = false)
    int appCode;

    public ApplicationPK() {
    }

    public ApplicationPK(String name, String family, String tel, int appCode) {
        this.name = name;
        this.family = family;
        this.tel = tel;
        this.appCode = appCode;
    }
    
}
