package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 *
 * @author rootnt
 */
@Embeddable
public class EducationOtherPK implements Serializable {
    @Column(name = "edu_order", nullable = false)
    int eduOrder;
    
    @Column(name = "app_code", nullable = false)
    int appCode;

    public EducationOtherPK() {
    }

    public EducationOtherPK(int eduOrder, int appCode) {
        this.eduOrder = eduOrder;
        this.appCode = appCode;
    }
    
        
}