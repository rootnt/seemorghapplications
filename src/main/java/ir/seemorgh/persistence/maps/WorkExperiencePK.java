package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author rootnt
 */
@Embeddable
public class WorkExperiencePK implements Serializable {
    @Column(name = "job_order", nullable = false)
    int jobOrder;
    
    @Column(name = "app_code", nullable = false)
    int appCode;

    public WorkExperiencePK() {
    }

    public WorkExperiencePK(int jobOrder, int appCode) {
        this.jobOrder = jobOrder;
        this.appCode = appCode;
    }
      
}
