package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author rootnt
 */
@Entity
@Table(name = "education_other")
public class EducationOtherEntity implements Serializable{
    @EmbeddedId
    EducationOtherPK id;
    
    @Column(name = "inst_name")
    String instName;
    
    @Column(name = "related", nullable = false)
    boolean related;
    
    @Column(name = "duration")
    int duration;
    
    @Column(name = "from_date")
    String fromDate;
    
    @Column(name = "to_date")
    String toDate;
    
    @Column(name = "course_subject", nullable = false)
    String courseSubject;

    public EducationOtherPK getId() {
        return id;
    }

    public void setId(EducationOtherPK id) {
        this.id = id;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }

    public boolean isRelated() {
        return related;
    }

    public void setRelated(boolean related) {
        this.related = related;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCourseSubject() {
        return courseSubject;
    }

    public void setCourseSubject(String courseSubject) {
        this.courseSubject = courseSubject;
    }
    
    
}
