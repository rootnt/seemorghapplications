package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author rootnt
 */
@Entity
@Table(name = "application")
public class ApplicationEntity implements Serializable {
    @EmbeddedId
    ApplicationPK id;
    
    @Column(name = "rcv_date", nullable = false)
    String rcvDate;
    
    @Column(name = "stage", nullable = false)
    short Stage;

    public ApplicationPK getId() {
        return id;
    }

    public void setId(ApplicationPK id) {
        this.id = id;
    }

    public String getRcvDate() {
        return rcvDate;
    }

    public void setRcvDate(String rcvDate) {
        this.rcvDate = rcvDate;
    }

    public short getStage() {
        return Stage;
    }

    public void setStage(short Stage) {
        this.Stage = Stage;
    }
        

}
