package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

/**
 *
 * @author rootnt
 */

@Entity
@Table(name = "eng_lvl")
public class EngLevelEntity implements Serializable {
    @Column(name = "lvl", nullable = false)
    int lvl;
    
    @Id
    @Column(name = "app_code", nullable = false)
    int appCode;

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getAppCode() {
        return appCode;
    }

    public void setAppCode(int appCode) {
        this.appCode = appCode;
    }
    
    
    
}
