package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

/**
 *
 * @author rootnt
 */

@Entity
@Table(name = "applicance")
public class ApplicanceEntity implements Serializable {
    @Id
    @Column(name = "app_code", nullable = false)
    int appCode;
    
    @Column(name = "resume_file")
    String resumeFile;

    public int getAppCode() {
        return appCode;
    }

    public void setAppCode(int appCode) {
        this.appCode = appCode;
    }

    public String getResumeFile() {
        return resumeFile;
    }

    public void setResumeFile(String resumeFile) {
        this.resumeFile = resumeFile;
    }
}
