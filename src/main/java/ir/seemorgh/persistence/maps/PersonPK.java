package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable 
public class PersonPK implements Serializable {
    @Column(name = "name", nullable = false)
    String name;
    @Column(name = "family", nullable = false)
    String family;
    @Column(name = "tel", nullable = false)
    String tel;

    public PersonPK() {
    }
    
    public PersonPK(String name, String family, String tel) {
        this.name = name;
        this.family = family;
        this.tel = tel;
    }
}