package ir.seemorgh.persistence.maps;

/**
 *
 * @author rootnt
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;


@Entity
@Table(name = "person")
public class PersonEntity implements Serializable {
    @EmbeddedId
    PersonPK id;
//    String name;
//    String family;
//    String tel;
    @Column(name = "national_code")
    String nationalCode;
    @Column(name = "brth_date")
    String brthDate;
    @Column(name = "addr")
    String addr;
    @Column(name = "email")
    String email;

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getBrthDate() {
        return brthDate;
    }

    public void setBrthDate(String brthDate) {
        this.brthDate = brthDate;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setId(String name, String family, String tel) {
        this.id = new PersonPK(name, family, tel);
//        return id;
    }
    
}


    