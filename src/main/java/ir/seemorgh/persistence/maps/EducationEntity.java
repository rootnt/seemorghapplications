package ir.seemorgh.persistence.maps;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author rootnt
 */
@Entity
@Table (name="education")
public class EducationEntity implements Serializable {
    @EmbeddedId
    EducationPK id;
    
    @Column(name = "uni_name")
    String uniName;
    
    @Column(name = "related")
    boolean related;
    
    @Column(name = "end_date")
    String end_date;
    
    @Column(name = "average")
    double average;

}
