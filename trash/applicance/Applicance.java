package ir.seemorgh.persistence.DAO.applicance;

import ir.seemorgh.persistence.maps.ApplicanceEntity;

/**
 *
 * @author rootnt
 */
public interface Applicance {
    public void addApplicance(ApplicanceEntity Applicance);
    public void deleteApplicance(ApplicanceEntity Applicance);
    public void updateApplicance(ApplicanceEntity Applicance);
    public ApplicanceEntity getApplicance(String name, String family, String tel);
}
