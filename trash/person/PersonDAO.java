package ir.seemorgh.persistence.DAO.person;


import ir.seemorgh.persistence.maps.PersonEntity;

/**
 *
 * @author rootnt
 */
public interface PersonDAO {
    public void addPerson(PersonEntity person);
    public void deletePerson(PersonEntity person);
    public void updatePerson(PersonEntity person);
    public PersonEntity getPerson(String name, String family, String tel);
}
