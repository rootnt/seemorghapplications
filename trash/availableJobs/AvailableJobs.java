package ir.seemorgh.persistence.DAO.availableJobs;

import ir.seemorgh.persistence.maps.AvailableJobsEntity;

/**
 *
 * @author rootnt
 */
public interface AvailableJobs {
        public void addJob(AvailableJobsEntity job);
    public void deleteJob(AvailableJobsEntity job);
    public void updateJob(AvailableJobsEntity job);
    public AvailableJobsEntity getJob(String name, String family, String tel);
}
