package ir.seemorgh.persistence.DAO.application;

import ir.seemorgh.persistence.maps.ApplicationEntity;



/**
 *
 * @author rootnt
 */
public interface ApplicationDAO {
    public void addApplication(ApplicationEntity application);
    public void deleteApplication(ApplicationEntity application);
    public void updateApplication(ApplicationEntity application);
    public ApplicationEntity getApplcation(String name, String family, String tel);
}
